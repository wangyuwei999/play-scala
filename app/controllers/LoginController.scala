package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
/**
  * Created by wineway on 6/20/17.
  */
@Singleton
class LoginController @Inject() extends Controller{
  import LoginController._
  def index = Action{
    //val tmp = views.html.login("login")(loginForm)
    Ok(views.html.index("login")(loginForm))
  }

  def action = Action{ request =>
    Ok(views.html.index("register")(regForm))
  }

  def submit = Action{ implicit request =>
    val formR = loginForm.bindFromRequest()

    formR.fold(
      hasErrors = {form: Form[loginClass] =>
        Ok(views.html.index("login")(form))
      },
      success = {login: loginClass =>
        dataBaseContorller.search(login) match{
          case true => Ok(views.html.index("成功登陆")(loginForm)).withSession("username" -> login.username)
          case false => Ok(views.html.index("登陆错误")(loginForm))
        }

      }
    )
  }

  def submit2 = Action{
    implicit request =>
      val formR = regForm.bindFromRequest()

      formR.fold(
        hasErrors = {
          form: Form[registerClass] =>
            Ok(views.html.index("register")(form))
        },
        success = {login: registerClass =>
          dataBaseContorller.add(login) match {
            case true => Ok(views.html.index("regSuc")(loginForm))
            case false => Ok(views.html.index("regFalt")(loginForm))
          }

        }
      )
  }

}
object LoginController {

  case class loginClass(username: String, password: String)
  implicit val loginForm = Form(mapping(
    "username" -> nonEmptyText,
    "password" -> nonEmptyText
  )(loginClass.apply)(loginClass.unapply))

  val blankForm = Form(mapping(
    "username" -> nonEmptyText,
    "password" -> nonEmptyText
  )(loginClass.apply)(loginClass.unapply))


  case class registerClass(username: String, passwd: String, email: String, area: String)
  implicit val regForm = Form(mapping(
    "username" -> nonEmptyText,
    "passwd" -> nonEmptyText,
    "email" -> email,
    "area" -> nonEmptyText
  )(registerClass.apply)(registerClass.unapply))

}