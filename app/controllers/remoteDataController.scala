package controllers

import javax.inject.Inject

import play.api.libs.ws._
import play.api.mvc._
import play.api.Play.current

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import play.api.libs.json._

/**
  * Created by wineway on 6/24/17.
  */
class remoteDataController @Inject() extends Controller {

  import controllers.remoteDataController._

  def remote = Action.async { request =>

    block()

    Future(Ok("ok"))

  }
}

object remoteDataController {



  def block() = {

    val baseTime: Long = 1498320611932L

    val response :Future[WSResponse] =
      WS.url("https://ali-pm25.showapi.com/pm25-top").
        withHeaders(("Authorization", "APPCODE " + "861739827ed24ed1b6ae11b197180f74")).
        get()


    response.map(_.json.\("showapi_res_body").\("list").
      as[JsArray].
      value.
      map(i => (i \ "pm2_5" , i \ "area")).
      map(i => (1, i._2.as[String], System.currentTimeMillis - baseTime, i._1.as[String]))).map(pm25Controller.addData(_))

  }
}

//    response.map(_.json).map(Ok(_))

//    response.map(_.json.\("showapi_res_body").\("list").
//      as[JsArray].
//      value.
//      map(i => (i \ "so2" , i \ "area_code")).
//      map(i => (i._1.as[String], i._2.as[String])).
//      map(i =>Json.obj((i._1,i._2)))).
//      map(Json.arr(_)).
//      map(Ok(_))
//  }
