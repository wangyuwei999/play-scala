package controllers

import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import javax.inject.Inject

import dataBaseContorller._

import slick.jdbc.PostgresProfile.api._
import pm25Controller._
import slick.lifted.TableQuery
import playconfig.MailerService
import anotherDatabaseController._

/**
  * Created by wineway on 6/25/17.
  */

class calcController @Inject() (mailServer: MailerService){



  def calcB() = {
    dataBaseContorller.db.run(useraTable.result).onSuccess{case pf: Seq[String] =>
    pf.map(findUsers(_))}
  }


  def findUsers(loc :String) ={

    val baseTime: Long = 1498320611932L

    val time = System.currentTimeMillis/3600 + 3

    dataBaseContorller.db.run(areaTable.filter(_.db_name === loc).map(i => (i.db_pm, i.db_time)).result).
      onSuccess { case dat: Seq[(String, Long)] =>

      val len = dat.length

      val calTmp = dat.map(s => (Option(s._1.trim.toInt).getOrElse(0), s._2/3600))

      val sum = calTmp.reduce((a, b) => ((a._1+b._1),(a._2+b._2)))

      val xy = calTmp.map(i => i._1*i._2).sum

      val x2 = calTmp.map(i => i._2*i._2)

      val b1 = (len*xy - sum._1*sum._2)/(len*x2.sum - sum._2)

      val b2 = sum._1/len - ((i: Long) => i * i)(b1*sum._2)

      if(((i:Long)=> b1*i+b2)(time) > 60){sendMail(loc, ((i:Long)=> b1*i+b2)(time))}
    }
      }




  def sendMail(loc: String, pm: Long) = {
    dataBaseContorller.db.run(userTable.filter(_.db_area === loc).map(i => (i.db_name, i.db_email)).result).onSuccess{case i :Seq[(String, String)] =>
        i.foreach(i => mailServer.sendTo(i._1, i._2, pm))
    }
  }

}
