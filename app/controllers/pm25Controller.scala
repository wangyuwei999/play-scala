package controllers

import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.duration._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
/**
  * Created by wineway on 6/24/17.
  */
object pm25Controller {

  val db = Database.forConfig("mydb")

  class area(tag: Tag) extends Table[(Int, String, Long, String)](tag, "area") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def db_name = column[String]("area_NAME") // This is the primary key column
    def db_time = column[Long]("area_TIME")
    def db_pm = column[String]("PM25")

    def * = (id ,db_name, db_time, db_pm)
  }
  val areaTable = TableQuery[area]

  val setup = DBIO.seq(areaTable.schema.create)
  val setupFuture = db.run(setup)
 // Await.result(setupFuture, 2 seconds)


  def addData(tup: Seq[(Int, String, Long, String)]) = db.run(areaTable ++= tup)

}
