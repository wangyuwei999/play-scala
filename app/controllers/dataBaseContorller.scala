package controllers

import play.api.libs.openid.Errors.AUTH_CANCEL
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.duration._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import anotherDatabaseController.useraTable
/**
  * Created by wineway on 6/21/17.
  */
object dataBaseContorller {

  val db = Database.forConfig("mydb")

  class User(tag: Tag) extends Table[(String, String, String, String)](tag, "USER") {
    def db_name = column[String]("USER_NAME", O.PrimaryKey) // This is the primary key column
    def db_password = column[String]("USER_PASSWD")
    def db_email = column[String]("EMAIL")
    def db_area = column[String]("area")

    def * = (db_name, db_password, db_email, db_area)
  }
  val userTable = TableQuery[User]

  val setup = DBIO.seq(userTable.schema.create)
  val setupFuture = db.run(setup)

  def add(obj :LoginController.registerClass) :Boolean= Await.result(Await.result(
  db.run{
    userTable.filter(c => c.db_name === obj.username).take(1).result
  }
    .map { s =>
      if (s.isEmpty) {
        db.run(DBIO.seq(
          userTable += (obj.username, obj.passwd, obj.email, obj.area),
          addArea(obj.area))
        ).map(_ => true)
      } else {
        db.run(DBIO successful false)
      }
    }, 2.seconds), 2.seconds)

  def search(obj: LoginController.loginClass):Boolean =Await.result(Await.result(
    db.run{
      userTable.filter(c => c.db_name === obj.username).take(1).result
    }
      .map { s =>db.run{
        if (s.isEmpty) {DBIO successful false}
        else if(s.head._2 == obj.password)
        {DBIO successful true}else{DBIO successful false}
      }
      }, 2.seconds), 2.seconds)

def add2(obj :LoginController.registerClass) :Future[Boolean]= {

   val ifExist = userTable.filter(c => c.db_name === obj.username).exists

   val tmpQ = Query((obj.username, obj.passwd, obj.email, obj.area)).filterNot(_ => ifExist)

   db.run(userTable.forceInsertQuery(tmpQ)).map{
     case 1 => true
     case _ => false
   }

}

  def addArea(obj :String) :DBIO[_]= {

    val ifExist = useraTable.filter(c => c === obj).exists

    val tmpQ = Query(obj).filterNot(_ => ifExist)

    useraTable.forceInsertQuery(tmpQ)

  }



 // }

}
/*
[error] o.p.d.c.BaseDataSource - Failed to create a Non-Pooling DataSource from PostgreSQL JDBC Driver 42.1.1 for wineway at jdbc:postgresql://localhost/mydb?loginTimeout=1: org.postgresql.util.PSQLException: FATAL: sorry, too many clients already
  Location: File: proc.c, Routine: InitProcess, Line: 340
  Server SQLState: 53300
[error] o.p.Driver - Connection error:
org.postgresql.util.PSQLException: FATAL: sorry, too many clients already
  Location: File: proc.c, Routine: InitProcess, Line: 340
  Server SQLState: 53300
	at org.postgresql.Driver$ConnectThread.getResult(Driver.java:401)
	at org.postgresql.Driver.connect(Driver.java:259)
	at java.sql.DriverManager.getConnection(DriverManager.java:664)
	at java.sql.DriverManager.getConnection(DriverManager.java:247)
	at org.postgresql.ds.common.BaseDataSource.getConnection(BaseDataSource.java:94)
	at org.postgresql.ds.common.BaseDataSource.getConnection(BaseDataSource.java:79)
	at com.zaxxer.hikari.pool.PoolBase.newConnection(PoolBase.java:341)
	at com.zaxxer.hikari.pool.PoolBase.newPoolEntry(PoolBase.java:193)
	at com.zaxxer.hikari.pool.HikariPool.createPoolEntry(HikariPool.java:430)
	at com.zaxxer.hikari.pool.HikariPool.access$500(HikariPool.java:64)
	*/