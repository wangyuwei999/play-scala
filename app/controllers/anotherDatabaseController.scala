package controllers

import controllers.dataBaseContorller._
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.duration._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}


/**
  * Created by wineway on 6/27/17.
  */
object anotherDatabaseController {

  class User_area(tag: Tag) extends Table[String](tag, "USER_area") {
    def db_area = column[String]("area")

    def * = (db_area)
  }
  val useraTable = TableQuery[User_area]

  val setup = DBIO.seq(useraTable.schema.create)
  val setupFuture = db.run(setup)


}


