package playconfig

/**
  * Created by wineway on 6/25/17.
  */
import play.api.libs.mailer._
import java.io.File
import play.api.mvc._

import org.apache.commons.mail.EmailAttachment
import javax.inject.{Inject, Singleton}

@Singleton
class MailerService @Inject() (mailerClient: MailerClient) extends Controller {
//      "Mister FROM <10134381@cumt.edu.cn>",
//      Seq("Miss TO <wangyuweihx@gmail.com>"),

  def sendTo(name: String, addr: String, num: Long) = {
    val cid = "1234"
    val email = Email(
      s"亲爱的用户$name",
      "Mister FROM <10134381@cumt.edu.cn>",
      Seq(s"Miss TO <$addr>"),
      bodyText = Some(s"预计三小时后pm2.5浓度达到$num,出行请佩戴口罩"),
      bodyHtml = Some("")
    )
    val id = mailerClient.send(email)
  }

  def send = Action {
    val cid = "1234"
    val email = Email(
      "Simple email",
      "Mister FROM <10134381@cumt.edu.cn>",
      Seq("Miss TO <wangyuweihx@gmail.com>"),
//      attachments = Seq(
//        AttachmentFile("favicon.png", new File(environment.classLoader.getResource("public/images/favicon.png").getPath), contentId = Some(cid)),
//        AttachmentData("data.txt", "data".getBytes, "text/plain", Some("Simple data"), Some(EmailAttachment.INLINE))
//      ),
      bodyText = Some("A text message"),
      bodyHtml = Some(s"""<html><body><p>An <b>html</b> message with cid <img src="cid:$cid"></p></body></html>""")
    )
    val id = mailerClient.send(email)
    Ok(s"Email $id sent!")
  }

}
object MailerService {

}