package task

import javax.inject.{Inject, Named, Singleton}

import akka.actor.{ActorRef, ActorSystem}
import controllers.calcController

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/**
  * Created by wineway on 6/26/17.
  */
@Singleton
class Schedule @Inject() (actorSystem: ActorSystem)(calc: calcController)(implicit executionContext: ExecutionContext) {

  actorSystem.scheduler.schedule(initialDelay = 10.seconds, interval = 30.minute){
     controllers.remoteDataController.block()
    calc.calcB()
  }


}
